/*author: Mehedi Hasan Shawon*/
package com.icc.cms.service;

import java.util.List;

import com.icc.cms.dto.User;

public interface LoginService {

	public boolean loginStatus(User user);
	
	public List<User> 	getAllUser();
}
