/*author: Mehedi Hasan*/
package com.icc.cms.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.icc.cms.entity.CountryEntity;

public interface CountryRepository extends JpaRepository<CountryEntity, Integer>{

}
