/*author: Mehedi Hasan*/
package com.icc.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="team_registration")
public class TeamRegistrationEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="team_id")
	private int 	teamId;
	@Column(name="country_id")
	private int 	countryId;
	@Column(name="team_member_name")
	private String 	teamMemberName;
	@Column(name="date_of_birth")
	private Date	dateOfBirth;
	@Column(name="age")
	private int		age;
	@Column(name="role")
	private String	role;
	
	public int getTeamId() {
		return teamId;
	}
	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getTeamMemberName() {
		return teamMemberName;
	}
	public void setTeamMemberName(String teamMemberName) {
		this.teamMemberName = teamMemberName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
