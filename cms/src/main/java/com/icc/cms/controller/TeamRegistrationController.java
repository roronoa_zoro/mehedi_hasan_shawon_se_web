/*author: Mehedi Hasan*/
package com.icc.cms.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.icc.cms.dto.Country;
import com.icc.cms.dto.TeamRegistration;
import com.icc.cms.dto.User;
import com.icc.cms.service.CountryService;
import com.icc.cms.service.TeamRegistrationService;

@Controller
public class TeamRegistrationController {
	TeamRegistrationService registrationService;
	CountryService			countryService;
	
	@Autowired
	public TeamRegistrationController(TeamRegistrationService registrationService, CountryService countryService) {
		super();
		this.registrationService = registrationService;
		this.countryService = countryService;
	}

	// populate all countries
	@ModelAttribute("countries")
	public List<Country> populateAllCountryList(){
		return this.countryService.getAllCountries();
	}
	
	// team registration getMapping
	@GetMapping("/team-registration-add.html")
	public String teamRegistrationAdd(Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
		TeamRegistration registration = new TeamRegistration();
		model.addAttribute("registration", registration);
		return "registration/team-registration-add";
		}
		else {
			model.addAttribute("user", new User());
			return "/login";
		}
	}
	
	// global success page getMapping
	@GetMapping("/team-registration-success.html")
	public String teamRegistrationSuccess() {
		return "registration/team-registration-success";
	}
	
	// save team registration postMapping
	@PostMapping("team-registration-add-save.html")
	public ModelAndView teamRegistrationAddSave(@Valid @ModelAttribute TeamRegistration registration, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		//System.out.println(registration.getRole());
		if (result.hasErrors()) {
			System.out.println(result);
			mav = new ModelAndView("registration/team-registration-add");
			mav.addObject("registration", registration);
			String errorMessage = "Invalid Input!!!";
			mav.addObject("errorMessage", errorMessage);
			return mav;
		}
		// save data into database 'team_registration'
		registrationService.addTeamRegistration(registration);
		// redirect to success page
		mav = new ModelAndView("redirect:/team-registration-success.html");
		return mav;
	}
	
	// delete team member from country
	@GetMapping("/team-registration-delete.html")
	public String deleteTeamRegistrationMember(Model model, @RequestParam("id") int teamId) {
		registrationService.deleteTeamRegistration(teamId);
		model.addAttribute("successMessage", "successfully deleted!!!");
		return "registration/team-registration-list";
	}
	
	// update team member getMapping
	@GetMapping("/team-registration-update.html")
	public String updateTeamRegistrationMember(Model model,HttpServletRequest request, @RequestParam("id") int teamId) {
		System.out.println(teamId);
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
			TeamRegistration registration = registrationService.getTeamRegistration(teamId);
			model.addAttribute("registration", registration);
			
			return "registration/team-registration-update";
		}
		else {
			model.addAttribute("user", new User());
			return "/login";
		}
	}
	
	// update data save postMapping
	@PostMapping("/team-registration-update-save.html")
	public ModelAndView updateSaveTeamRegistrationMember(@Valid @ModelAttribute TeamRegistration registration, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println(result);
			mav = new ModelAndView("registration/team-registration-add");
			mav.addObject("registration", registration);
			String errorMessage = "Invalid Input!!!";
			mav.addObject("errorMessage", errorMessage);
			return mav;
		}
		// save data to database 'team_registration'
		registrationService.updateTeamRegistration(registration);
		// redirect to global success page
		mav = new ModelAndView("redirect:/team-registration-success.html");
		return mav;
	}
	
	// all list of team member page show
	@GetMapping("team-registration-list.html")
	public String teamMemberListShow(Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
		return "registration/team-registration-list";
		}
		else {
			model.addAttribute("user", new User());
			return "/login";
		}
	}
	
	// show only country wise team member
	@ResponseBody
	@PostMapping("getCountryWiseTeam")
	public List<TeamRegistration> getCountryWiseTeam(@RequestParam int countryId){
		System.out.println(countryId);
		List<TeamRegistration> list = new ArrayList<TeamRegistration>();
		for (TeamRegistration registration : registrationService.getAllTeamRegistrations()) {
			if (registration.getCountryId() == countryId) {
				list.add(registration);
			}
		}
		return list;
												
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
