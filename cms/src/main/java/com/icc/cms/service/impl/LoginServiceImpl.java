/*author: Mehedi Hasan Shawon*/
package com.icc.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icc.cms.dto.User;
import com.icc.cms.entity.UserEntity;
import com.icc.cms.repo.UserRepository;
import com.icc.cms.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{
	@Autowired
	UserRepository userRepository;



	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		List<UserEntity> storedAllUsers = userRepository.findAll();
		List<User> allUserList = new ArrayList<User>();
		for (UserEntity entity : storedAllUsers) {
			User user = new User();
			BeanUtils.copyProperties(entity, user);
			allUserList.add(user);
		}
		
		return allUserList;
	}
	
	@Override
	public boolean loginStatus(User user) {
		// TODO Auto-generated method stub
		for (User dto : getAllUser()) {
			if(user.getUserName().equals(dto.getUserName())&& user.getUserPassword().equals(dto.getUserPassword())) {
				return true;
			}
				
		}
		return false;
	}

}
