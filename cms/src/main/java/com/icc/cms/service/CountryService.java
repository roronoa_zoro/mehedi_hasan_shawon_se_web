/*author: Mehedi Hasan*/
package com.icc.cms.service;

import java.util.List;

import com.icc.cms.dto.Country;

public interface CountryService {
	public List<Country>	getAllCountries();
	
	public Country			getCountry(int countryId);
	
	public void				addNewCountry(Country country);
	
	public void				deleteCountry(int countryId);
	
	public	Country			updateCountry(Country country);
	
	boolean					matchCountryName(String countryName);
}
