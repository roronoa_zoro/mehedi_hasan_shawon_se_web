/*author: Mehedi Hasan*/
package com.icc.cms.service;

import java.util.List;

import com.icc.cms.dto.TeamRegistration;

public interface TeamRegistrationService {
	public List<TeamRegistration>		getAllTeamRegistrations();
	
	public TeamRegistration				getTeamRegistration(int teamId);
	
	public void							addTeamRegistration(TeamRegistration registration);
	
	public TeamRegistration				updateTeamRegistration(TeamRegistration registration);
	
	public void							deleteTeamRegistration(int teamId);
}
