/*author: Mehedi Hasan Shawon*/
package com.icc.cms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.icc.cms.dto.User;
import com.icc.cms.service.LoginService;

@Controller
public class UserController {
	@Autowired
	LoginService loginService;
	
	// show login page
	@GetMapping(value= {"/","/login.html"})
	public String loginShow(Model model) {
		model.addAttribute("user", new User());
		return "/login";
	}
	
	// valid user entry
	@PostMapping("/userLogin.html")
	public ModelAndView loginSuccess(User user, HttpSession session) {
		ModelAndView mav = new ModelAndView();
		boolean bool = loginService.loginStatus(user);
		if(bool == true) {
			session.setAttribute("user", user.getUserName());
			mav.setViewName("/welcome"); 
		}
		else {
			mav.setViewName("/login");
			mav.addObject("errorMessage", "Invalid user name and password !!");
		}
		 
		return mav;
	}
	
	// session kill after logout
	@GetMapping("logOut")
	public String logOut(HttpServletRequest request,Model model){
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
			session.invalidate();
		}
		model.addAttribute("user", new User());
		return "/login";
	}
}
