/*author: Mehedi Hasan*/
package com.icc.cms.dto;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class TeamRegistration {
	private int 	teamId;
	@Min(value = 1, message="select country first !!!")
	private int 	countryId;
	@Size(min = 2, max = 30, message = "Member Name should be within 30 characters")
	private String 	teamMemberName;
	
	private Date	dateOfBirth;
	@Min(value = 15, message="age should be more than 15")
	private int		age;
	
	private String	role = "Player";

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getTeamMemberName() {
		return teamMemberName;
	}

	public void setTeamMemberName(String teamMemberName) {
		this.teamMemberName = teamMemberName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "TeamRegistration [teamId=" + teamId + ", countryId=" + countryId + ", teamMemberName=" + teamMemberName
				+ ", dateOfBirth=" + dateOfBirth + ", age=" + age + ", role=" + role + "]";
	}
	
	
}
