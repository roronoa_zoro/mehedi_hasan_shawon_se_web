/*author: Mehedi Hasan*/
package com.icc.cms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.icc.cms.dto.Country;
import com.icc.cms.dto.User;
import com.icc.cms.service.CountryService;

@Controller
public class CountryController {
	
	CountryService service;
	
	@Autowired
	public CountryController(CountryService service) {
		super();
		this.service = service;
	}

	/* <=== country add page getMapping ===> */
	@GetMapping("/country-add.html")
	public String countryAdd(Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
			Country country = new Country();
			model.addAttribute("country", country);
			return "country/country-add";
		}
		else {
			model.addAttribute("user", new User());
			return "/login";
		}
		
	}
	
	/* <=== global success page getMapping ===> */
	@GetMapping("country-add-success.html")
	public String countryAddSuccess() {
		return "country/country-add-success";
	}
	
	/* <=== country save postMapping ===> */
	@PostMapping("/country-add-save.html")
	public ModelAndView countryAddSave(@Valid @ModelAttribute Country country, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		
		if (result.hasErrors()) {
			mav = new ModelAndView("country/country-add");
			mav.addObject("country", country);
			mav.addObject("errorMessage", "Invalid Input!!");
			return mav;
		}
		
		// country data is add to the database 'country'
		service.addNewCountry(country);
		// redirect to the global success page so that same data can't enter to the db while reloading the entire page
		mav = new ModelAndView("redirect:/country-add-success.html");
		
		return mav;
		
	}
	
	/* <===  country list page getMapping ===> */
	@GetMapping("/country-list.html")
	public String allCountryList(Model model,HttpServletRequest request) {
		
		
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
			model.addAttribute("countries", service.getAllCountries());
			return "country/country-list";
		}
		else {
			model.addAttribute("user", new User());
			return "/login";
		}
		
	}
	
	/* <===  country delete page getMapping ===> */
	@GetMapping("country-delete.html")
	public String countryDelete(Model model, @RequestParam int countryId) {
		service.deleteCountry(countryId);
		model.addAttribute("countries", service.getAllCountries());
		model.addAttribute("successMessage", "Successfully Deleted");
		return "country/country-list";
	}
	
	/* <===  country update page getMapping ===> */
	@GetMapping("country-update.html")
	public String countryUpdate(Model model,HttpServletRequest request, @RequestParam int countryId) {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null) {
		Country country = service.getCountry(countryId);
		model.addAttribute("country", country);
		return "country/country-update";
		}
		else {
			model.addAttribute("user", new User());
			return "/login";
		}
	}
	
	/* <===  country update page save postMapping ===> */
	@PostMapping("/country-update-save.html")
	public ModelAndView countryUpdateSave(@Valid @ModelAttribute Country country, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		
		if (result.hasErrors()) {
			mav = new ModelAndView("country/country-add");
			mav.addObject("country", country);
			mav.addObject("errorMessage", "Invalid Input!!");
			return mav;
		}
		
		// country data update to the database 'country'
		service.updateCountry(country);
		
		// redirect to the global success page so that same data can't enter to the db while reloading the entire page
		mav = new ModelAndView("redirect:/country-add-success.html");
		
		return mav;
		
	}
	
	/* finding the duplicate country name with ajex call */
   	@ResponseBody
	@PostMapping("getCountryName")
   public String getCountryName(@RequestParam String countryName){
	   String str_countryName = countryName.toLowerCase().replaceAll("\\s", "");
	   boolean bool = service.matchCountryName(str_countryName);
	   String str_result = " ";
	   if (bool== true) {
			str_result = "Country Name exists";
		}else{
			str_result = "No";
		}
	   return str_result;
   }
	
	
	
	
	
	
	
}
