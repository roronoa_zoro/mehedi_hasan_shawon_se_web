/*author: Mehedi Hasan*/
package com.icc.cms.dto;

import javax.validation.constraints.Size;

public class Country {
	private int 	countryId;
	@Size(min=1, max= 30, message="Name should be within 30 characters.")
	private String	countryName;

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		return "Country [countryId=" + countryId + ", countryName=" + countryName + "]";
	}
	
	
}
