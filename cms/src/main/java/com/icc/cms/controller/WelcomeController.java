/*author: Mehedi Hasan Shawon*/
package com.icc.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {
	@GetMapping("/welcome.html")
	public String welcomePageShow() {
		return "welcome";
	}
	
	
	
}
