/*author: Mehedi Hasan*/
package com.icc.cms.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icc.cms.dto.Country;
import com.icc.cms.dto.TeamRegistration;
import com.icc.cms.entity.CountryEntity;
import com.icc.cms.repo.CountryRepository;
import com.icc.cms.service.CountryService;
import com.icc.cms.service.TeamRegistrationService;

@Service
public class CountryServiceImpl implements CountryService{
	
	CountryRepository 			countryRepository;
	TeamRegistrationService		registrationService;
	
	@Autowired
	public CountryServiceImpl(CountryRepository countryRepository, TeamRegistrationService registrationService) {
		super();
		this.countryRepository = countryRepository;
		this.registrationService = registrationService;
	}
	
	/* all country list */
	@Override
	public List<Country> getAllCountries() {
		List<CountryEntity> storedCountries = countryRepository.findAll();
		
		List<Country> countryList = new ArrayList<>(storedCountries.size());
		for(CountryEntity entity : storedCountries) {
			Country dto = new Country();
			BeanUtils.copyProperties(entity, dto);
			countryList.add(dto);
		}
		
		return countryList;
	}

	/* get one country info by id */
	@Override
	public Country getCountry(int countryId) {
		CountryEntity entity = countryRepository.findById(countryId).get();
		System.out.println(entity);
		
		if (entity == null) {
			return null;
		}else {
			Country dto = new Country();
			BeanUtils.copyProperties(entity, dto);
			return dto;
		}
		
	}
	
	/* add new country method */
	@Override
	public void addNewCountry(Country country) {
		CountryEntity entity = new CountryEntity();
		BeanUtils.copyProperties(country, entity);
		countryRepository.save(entity);
	}
	/* delete country method*/
	@Override
	public void deleteCountry(int countryId) {
		countryRepository.deleteById(countryId);
		for (TeamRegistration registration : registrationService.getAllTeamRegistrations()) {
			if (registration.getCountryId() == countryId) {
				registrationService.deleteTeamRegistration(registration.getTeamId());;
			}
		}
		
	}
	/* update country method*/
	@Override
	public Country updateCountry(Country country) {
		
		CountryEntity entity = new CountryEntity();
		BeanUtils.copyProperties(country, entity);
		countryRepository.save(entity);
		BeanUtils.copyProperties(entity, country);
		
		return country;
	}
	
	
	// duplication condition

	@Override
	public boolean matchCountryName(String countryName) {
		// TODO Auto-generated method stub
		Optional<CountryEntity> countryEntity = countryRepository.findAll().stream()
														.filter(p -> p.getCountryName().toLowerCase().replaceAll("\\s", "").equals(countryName))
														.findFirst();
		if (countryEntity.isPresent()) {
			return true;
		}else{
		return false;
		}
	}
}
