/*author: Mehedi Hasan Shawon*/
package com.icc.cms.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.icc.cms.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

}
