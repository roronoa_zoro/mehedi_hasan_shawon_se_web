/*author: Mehedi Hasan*/
package com.icc.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icc.cms.dto.TeamRegistration;
import com.icc.cms.entity.TeamRegistrationEntity;
import com.icc.cms.repo.TeamRegistrationRepository;
import com.icc.cms.service.TeamRegistrationService;

@Service
public class TeamRegistrationServiceImpl implements TeamRegistrationService{
	
	TeamRegistrationRepository repository;
	
	@Autowired
	public TeamRegistrationServiceImpl(TeamRegistrationRepository repository) {
		super();
		this.repository = repository;
	}
	
	// get all team member list method
	@Override
	public List<TeamRegistration> getAllTeamRegistrations() {
		List<TeamRegistrationEntity> storedAllTeamRegistrations = repository.findAll();
		
		List<TeamRegistration> teamRegistrationList = new ArrayList<TeamRegistration>(storedAllTeamRegistrations.size());
		
		for (TeamRegistrationEntity teamRegistrationEntity : storedAllTeamRegistrations) {
			TeamRegistration dto = new TeamRegistration();
			BeanUtils.copyProperties(teamRegistrationEntity, dto);
			teamRegistrationList.add(dto);
		}
		return teamRegistrationList;
		
	}
	
	// get one team member method
	@Override
	public TeamRegistration getTeamRegistration(int teamId) {
		// TODO Auto-generated method stub
		TeamRegistrationEntity entity = repository.findById(teamId).get();
		
		if (entity == null) {
			return null;
		}else {
		TeamRegistration dto = new TeamRegistration();
		BeanUtils.copyProperties(entity, dto);
		return dto;
		}
	}
	
	// team member add method
	@Override
	public void addTeamRegistration(TeamRegistration registration) {
		// TODO Auto-generated method stub
		TeamRegistrationEntity entity = new TeamRegistrationEntity();
		BeanUtils.copyProperties(registration, entity);
		repository.save(entity);
		
	}
	
	// team member update method
	@Override
	public TeamRegistration updateTeamRegistration(TeamRegistration registration) {
		// TODO Auto-generated method stub
		TeamRegistrationEntity entity = new TeamRegistrationEntity();
		BeanUtils.copyProperties(registration, entity);
		repository.save(entity);
		BeanUtils.copyProperties(entity, registration);
		
		return registration;
	}
	
	// team member delete method
	@Override
	public void deleteTeamRegistration(int teamId) {
		// TODO Auto-generated method stub
		repository.deleteById(teamId);
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
