/*author: Mehedi Hasan*/
package com.icc.cms.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.icc.cms.entity.TeamRegistrationEntity;

public interface TeamRegistrationRepository extends JpaRepository<TeamRegistrationEntity, Integer>{

}
